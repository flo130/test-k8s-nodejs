Installation for development
----------------------------

* `npm install`
* `npm run start` # to start prod mode
* `npm run start:dev` # to start dev mode

Then go to http://localhost:3000

Build and run the docker image
------------------------------

* `docker build -t <username>/test-k8s-nodejs .` # build image
* `docker run -p 3000:3000 -d <username>/test-k8s-nodejs` # launch server on the 3000 port

Then go to http://localhost:3000

CI working
----------

In all cases we build the Docker image

* When committing to the `master` branch, deployment will be done on an URL like `nodejs.xxxx`
* When committing to the `develop` branch, deployment will be done on an URL like `dev-<gitProjectName>.xxxx`
* When committing to a `feature/*` branch, deployment will be done an URL like `feature-<gitProjectName>.xxxx`

Kubernetes deployment
---------------------

* Creation of an `ingress` which will match a domain name and redirect to the service on port 80
* Creation of a loadbalancer type `service` that will retrieve requests to transmit them to pods on port 3000
* Creation of a `deployment` from the Docker image (generated beforehand by the CI job) on two replicas (two pods) listening on port 3000
