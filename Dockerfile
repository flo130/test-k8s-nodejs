FROM node:10
WORKDIR /usr/src/app
COPY . .
RUN npm install --verbose
EXPOSE 3000
CMD [ "npm", "start" ]
